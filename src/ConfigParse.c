/*
 *  This file is part of w3panik
 *
 *  w3panik - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "ConfigParse.h"

section_t* config_init() {
    section_t* sec_ret = (section_t*) malloc(sizeof(section_t));
    sec_ret->section_size = 0;
    sec_ret->option_list_start = NULL;
    return sec_ret;
}

void cfgsection_load(FILE* fconfig, section_t* cfg, char* section_name) {
    char snamestr[11] = "[";
    strcat(snamestr, section_name);
    strcat(snamestr, "]");

    size_t nbytes = 0;
    char* gotline = NULL;
    char* delim = "=\n";
    char* delim2 = "\n";
    while (getline(&gotline, &nbytes, fconfig) != -1) {
        if (strstr(gotline, snamestr)) {
            // keep reading until we reach OEF or a new section
            while (getline(&gotline, &nbytes, fconfig) != -1 && !(strstr(gotline, "]"))) {
                // I'm not exactly proud of this, but it'll do for now
                if (strstr(gotline, "=") && !(strstr(gotline, "#"))) {
                    char* cpy = strdup(gotline);
                    char* key = strtok(cpy, delim);
                    char* val = strtok(NULL, delim2);
                    config_append(cfg, key, val);
                    free(cpy);
                }
            }
        }
    }
    free(gotline);
    gotline = NULL;
}

void config_append(section_t* cfg, char* key, char* value) {
    if (cfg->section_size == 0) {
        cfg->option_list_start = (option_t*) malloc(sizeof(option_t));

        cfg->option_list_start->key = strdup(key);
        cfg->option_list_start->value = strdup(value);

        cfg->option_list_start->next = NULL;
        cfg->section_size = 1;
        return;
    } else {
        option_t* current = cfg->option_list_start;
        while (current->next != NULL) {
            current = current->next;
        }

        current->next = (option_t*) malloc(sizeof(option_t));
        current->next->key = strdup(key);
        current->next->value = strdup(value);

        current->next->next = NULL;
        cfg->section_size += 1;
        return;
    }
}

char* config_get(section_t* cfg, char* key) {
    option_t* current = cfg->option_list_start;
    while (current != NULL) {
        if (strcmp(current->key, key) != 0) {
            current = current->next;
            continue;
        } else {
            return current->value;
        }
    }

    return NULL; // we found nothing
}

void config_dealloc(section_t* cfg) {
    /*
     * Free all resources in config
     * and set its pointer to NULL
     */
    option_t* next = NULL;

    while (cfg->option_list_start != NULL) {
        next = cfg->option_list_start->next;

        /*
         * we deallocate the option_t as well as
         * its strings allocated by strdup()
         */
        free(cfg->option_list_start->key);
        free(cfg->option_list_start->value);
        free(cfg->option_list_start);

        cfg->option_list_start = next;
    }

    free(cfg);
    cfg = NULL;
}
