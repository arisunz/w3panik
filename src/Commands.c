/*
 *  This file is part of w3panik
 *
 *  w3panik - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Commands.h"

uint32_t command_check(char* klist[], char* clist[], size_t list_size, char* buffer) {
    for (uint32_t i = 0; i < list_size; ++i) {
        /*
         * we use strcasestr() here, which is non-standard
         * but in turn it will save us some headaches
         * since we can compare strings ignoring their case
         */
        if (strcasestr(buffer, klist[i]) != NULL) {
            command_execute(clist[i]);
            return 0; // command found OK, caller should clear buffer
        }
    }

    return 1;
}

int32_t command_execute(char* command) {
    return system(command);
}

void command_list_append(
    char* klist[], char* clist[], size_t* list_size,
    char* keyword, char* command) {

    klist[*list_size] = keyword;
    clist[*list_size] = command;
    *list_size += 1;
}
