/*
 *  This file is part of w3panik
 *
 *  w3panik - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef COMMANDS_H
#define COMMANDS_H
#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

uint32_t command_check(char* klist[], char* clist[], size_t list_size, char* buffer);

int32_t command_execute(char* command);

void command_list_append(char* klist[], char* clist[], size_t* list_size,
                         char* keyword, char* command);

#endif
