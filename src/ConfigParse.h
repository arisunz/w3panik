/*  
 *  This file is part of w3panik
 * 
 *  w3panik - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

#ifndef CONFIGPARSE_H
#define CONFIGPARSE_H
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

typedef struct option_t {
    char* key;
    char* value;
    struct option_t* next;
} option_t;

typedef struct section_t {
    uint32_t section_size;
    option_t* option_list_start;
} section_t;

section_t* config_init();
void config_append(section_t* cfg, char* key, char* value);
char* config_get(section_t* cfg, char* key);
void config_dealloc(section_t* cfg);
void cfgsection_load(FILE* fconfig, section_t* cfg, char* section_name);

#endif
