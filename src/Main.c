/*
 *  This file is part of w3panik
 *
 *  w3panik - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <linux/input.h>
#include "Keybuffer.h"
#include "Commands.h"
#include "ConfigParse.h"

//#define KEY_Ñ 39

// disgusting, gotta find a better way
static int DAEMON_RUNNING = 1;

void kclist_populate(section_t* commands, size_t* list_size, char* klist[], char* clist[]);
unsigned char event_check(uint32_t value);
void graceful_shutdown(int sign);

int main(int argc, char* argv[]) {
    if (getuid() != 0) {
        fprintf(stderr, "For simplicity and security's sake, this programm needs to be run as root.\n");
        return EXIT_FAILURE;
    }

    FILE* fconfig = fopen("/etc/w3panik/w3panik.conf", "r");
    if (fconfig == NULL) {
        perror("Fatal error while trying to read configuration file");
        fprintf(stderr, "Check for its presence at '/etc/w3panik/w3panik.conf' and try again.");
        return EXIT_FAILURE;
    }
    section_t* options = config_init();
    section_t* commands = config_init();

    cfgsection_load(fconfig, options, "OPTIONS");
    rewind(fconfig);
    cfgsection_load(fconfig, commands, "COMMANDS");

    // done reading config sections
    fclose(fconfig);

    fprintf(stderr, "%s - %s\n", commands->option_list_start->key, commands->option_list_start->value);

    if (commands->option_list_start == NULL) {
        fprintf(stderr, "No commands specified!\n");
        config_dealloc(options);
        config_dealloc(commands);
        return EXIT_FAILURE;
    }

    char event_path[20] = "/dev/input/event";
    char* nevent = config_get(options, "input_device");
    if (nevent != NULL) {
        strcat(event_path, nevent);
    } else {
        fprintf(stderr, "Failed to load event device from config...\n");
        config_dealloc(options);
        config_dealloc(commands);
        return EXIT_FAILURE;
    }
    fprintf(stderr, "%s\n", event_path);

    keydeque* key_deque;
    char* buffer_size = config_get(options, "buffer_size");
    if (buffer_size != NULL) {
        key_deque = buffer_allocate(atoi(buffer_size));
    } else {
        key_deque = buffer_allocate(32);
    }

    /*
     * sort of an ugly hack to have dynamic associative arrays
     * with these we store keys and their corresponding commands
     */
    char* klist[commands->section_size];
    char* clist[commands->section_size];
    size_t list_size = 0;

    kclist_populate(commands, &list_size, klist, clist);

    FILE* event_stream = fopen(event_path, "rb");
    if (event_stream != NULL) {
        fprintf(stderr, "Event stream opened!\n");
    } else {
        perror("Fatal error");
        free(key_deque);
        config_dealloc(options);
        config_dealloc(commands);
        return EXIT_FAILURE;
    }

    struct sigaction term;
    memset(&term, 0, sizeof(struct sigaction));
    term.sa_handler = graceful_shutdown;
    sigaction(SIGTERM, &term, NULL);

    fprintf(stderr, "Lift off!\n");
    int32_t daemonize_rc = daemon(0, 0); // we don't need much more than this at the moment
    if (daemonize_rc == -1) {
        perror("Fatal error");
        goto DAEMON_FAIL_CLEANUP;
    }

    struct input_event* ev = (struct input_event*) malloc(sizeof(struct input_event));
    while (fread(ev, sizeof(struct input_event), 1, event_stream)) {
        if (DAEMON_RUNNING == 0) {
            break;
        }

        if (ev->value == 1) {
            unsigned char ch_code = event_check(ev->code);
            if (ch_code != ' ') {
                buffer_append(key_deque, ch_code);
                if (command_check(klist, clist, list_size, key_deque->buffer) == 0) {
                    buffer_clear(key_deque);
                }
            }
        }
    }

    free(ev);

  DAEMON_FAIL_CLEANUP:
    free(key_deque);

    config_dealloc(options);
    config_dealloc(commands);

    fclose(event_stream);

    return EXIT_SUCCESS;
}

void graceful_shutdown(int sign) {
    DAEMON_RUNNING = 0;
}

void kclist_populate(section_t* commands, size_t* list_size, char* klist[], char* clist[]) {
    option_t* current = commands->option_list_start;
    while (current != NULL) {
        command_list_append(klist, clist, list_size, current->key, current->value);
        current = current->next;
    }
}

unsigned char event_check(uint32_t value) {
    switch (value) {
        case KEY_1: return '1';
        case KEY_2: return '2';
        case KEY_3: return '3';
        case KEY_4: return '4';
        case KEY_5: return '5';
        case KEY_6: return '6';
        case KEY_7: return '7';
        case KEY_8: return '8';
        case KEY_9: return '9';
        case KEY_0: return '0';
        case KEY_Q: return 'Q';
        case KEY_W: return 'W';
        case KEY_E: return 'E';
        case KEY_R: return 'R';
        case KEY_T: return 'T';
        case KEY_Y: return 'Y';
        case KEY_U: return 'U';
        case KEY_I: return 'I';
        case KEY_O: return 'O';
        case KEY_P: return 'P';
        case KEY_A: return 'A';
        case KEY_S: return 'S';
        case KEY_D: return 'D';
        case KEY_F: return 'F';
        case KEY_G: return 'G';
        case KEY_H: return 'H';
        case KEY_J: return 'J';
        case KEY_K: return 'K';
        case KEY_L: return 'L';
        //case KEY_Ñ: return 'Ñ';
        case KEY_Z: return 'Z';
        case KEY_X: return 'X';
        case KEY_C: return 'C';
        case KEY_V: return 'V';
        case KEY_B: return 'B';
        case KEY_N: return 'N';
        case KEY_M: return 'M';
        default: return ' ';
    }
}
