/*  
 *  This file is part of w3panik
 * 
 *  w3panik - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

#include "Keybuffer.h"

keydeque* buffer_allocate(size_t buffer_size) {
    keydeque* ret = (keydeque*) malloc(sizeof(keydeque) + buffer_size + 1);

    ret->buffer_size = buffer_size;
    for (uint32_t i = 0; i < buffer_size; ++i) {
        ret->buffer[i] = '0';
    }

    ret->buffer[buffer_size] = '\0';

    return ret;
}

void buffer_append(keydeque* key_deque, char ch) {
    for (uint32_t i = 0; i < key_deque->buffer_size; ++i) {
        key_deque->buffer[i] = key_deque->buffer[i + 1];
    }

    key_deque->buffer[key_deque->buffer_size - 1] = ch;
}

void buffer_clear(keydeque* key_deque) {
    for (uint32_t i = 0; i < key_deque->buffer_size; ++i) {
        key_deque->buffer[i] = '0';
    }
}
