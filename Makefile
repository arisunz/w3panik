OBJ_DEBUG_MAIN = build/debug/objs/Main.o
OBJ_DEBUG_COMM = build/debug/objs/Commands.o
OBJ_DEBUG_KBFF = build/debug/objs/Keybuffer.o
OBJ_DEBUG_CFGP = build/debug/objs/ConfigParse.o

OBJ_RELEASE_MAIN = build/release/objs/Main.o
OBJ_RELEASE_COMM = build/release/objs/Commands.o
OBJ_RELEASE_KBFF = build/release/objs/Keybuffer.o
OBJ_RELEASE_CFGP = build/release/objs/ConfigParse.o

OBJS_DEBUG = $(OBJ_DEBUG_MAIN) $(OBJ_DEBUG_COMM) $(OBJ_DEBUG_KBFF) $(OBJ_DEBUG_CFGP)
OBJS_RELEASE = $(OBJ_RELEASE_MAIN) $(OBJ_RELEASE_COMM) $(OBJ_RELEASE_KBFF) $(OBJ_RELEASE_CFGP)

CC = gcc

FLAGS_BASE = -Wall
FLAGS_DEBUG = -g
FLAGS_RELEASE = -O3

BINARY = w3panik

debug : $(OBJS_DEBUG)
	$(CC) $(FLAGS_DEBUG) -o $(BINARY) $(OBJS_DEBUG)

release : $(OBJS_RELEASE)
	$(CC) $(FLAGS_RELEASE) -o $(BINARY) $(OBJS_RELEASE)

build/debug/objs/Main.o: src/Main.c
	$(CC) $(FLAGS_DEBUG) -o $(OBJ_DEBUG_MAIN) -c src/Main.c

build/debug/objs/Commands.o: src/Commands.c src/Commands.h
	$(CC) $(FLAGS_DEBUG) -o $(OBJ_DEBUG_COMM) -c src/Commands.c

build/debug/objs/Keybuffer.o: src/Keybuffer.c src/Keybuffer.h
	$(CC) $(FLAGS_DEBUG) -o $(OBJ_DEBUG_KBFF) -c src/Keybuffer.c

build/debug/objs/ConfigParse.o: src/ConfigParse.c src/ConfigParse.h
	$(CC) $(FLAGS_DEBUG) -o $(OBJ_DEBUG_CFGP) -c src/ConfigParse.c


build/release/objs/Main.o: src/Main.c
	$(CC) $(FLAGS_RELEASE) -o $(OBJ_RELEASE_MAIN) -c src/Main.c

build/release/objs/Commands.o: src/Commands.c src/Commands.h
	$(CC) $(FLAGS_RELEASE) -o $(OBJ_RELEASE_COMM) -c src/Commands.c

build/release/objs/Keybuffer.o: src/Keybuffer.c src/Keybuffer.h
	$(CC) $(FLAGS_RELEASE) -o $(OBJ_RELEASE_KBFF) -c src/Keybuffer.c

build/release/objs/ConfigParse.o: src/ConfigParse.c src/ConfigParse.h
	$(CC) $(FLAGS_RELEASE) -o $(OBJ_RELEASE_CFGP) -c src/ConfigParse.c

clean-all:
	-rm -f $(OBJS_DEBUG) $(OBJS_RELEASE) $(BINARY)

clean-release:
	-rm -f $(OBJS_RELEASE) $(BINARY)

clean-debug:
	-rm -f $(OBJS_DEBUG) $(BINARY)
