# w3panik

Keyboard sequence listener daemon.

`w3panik` sits in the background listening for certain keyboard sequences (specified by the user) and, upon detecting one, it will react by calling its corresponding shell command (also specified by the user). This is achieved by reading `/dev/input/event*` into a (small) buffer. Because of this and the nature of `w3panik`, it must be executed with root privileges.

Although multipurpose, this program is developed with critical and delicate situations in mind. If you're looking for a shortcut daemon, you're better off with an actual shortcut daemon, because this is not it.

Or you could use it to yell at you every time you type something nasty. Your choice.


## Usage
First, find out your keyboard event file by running `ls -l /dev/input/by-id`. You'll see files pointing to `../event*`. The one containing "keyboard" or "kbd" should be pointing to what you need.

Then you need to edit the configuration file (which should be located at `/etc/w3panik/w3panik.conf`) with `input_device` set to the number corresponding to your keyboard's input device. It is recommended you use [the provided config file](w3panik.conf) as an example:

```
# Example configuration
#
# Both OPTIONS and COMMANDS sections are mandatory
#

[OPTIONS]
# You'll need to specify an input_device
#
# buffer_size specifies the size of ther buffer used to
# store keypresses. It is recommended you keep it only as
# large as you need, even though it does get cleared after
# executing a command.
#
input_device=7
buffer_size=16

[COMMANDS]
# It is important that there's no spaces around the '='
#
# Note that keywords are *not* case sensitive.
#
example=notify-send example
# needless to say, you should be careful with these
poweroffnow=poweroff
```

Now you can run it and it'll launch as a daemon. It will respond to `SIGTERM` and shutdown gracefully.


## Building


### Requirements
* Recent GCC version in a Linux system.
* Your tipycal build essentials.
* Somewhat recent version of glibc -- we use some non-standard features.
* Linux input subsystem headers -- you probably have these already.

From there, do either `make debug` or `make release` and you can run it `sudo ./w3panik` (preferably after configuring it).

Unfortuately, this is Linux dependant and I don't have the time nor resources to port to other systems.


## Contributing
Feel free open an issue. Or send me a patch. While doing so, please try to adhere to the used coding style.


### To Do (in urgency order):
* Improve daemon-like behaviour. (maybe implement logging?)
* Implement proper Unicode support.


## License

(C) 2018 - Ariela Wenner

This project is licensed under the GNU GPLv3. See the [LICENSE](LICENSE.md) for details.
